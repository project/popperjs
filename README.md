# For developers

## Installation

```

composer require drupal/popperjs

```

## Drupal 9 to Drupal 10 migrations

In *.libraries.yml replace `core/popperjs` with `popperjs/popper.js`.

### Example

In your module or theme add `popperjs` as a dependency.

```
formatter:
  version: VERSION
  js:
    js/bynder.formatter.js: {}
  css:
    theme:
      css/bynder.formatter.css: {}
  dependencies:
    - core/drupal
    - core/once
    - popperjs/popperjs
```

# Maintainers

Update package instructions:

```
$ npm install
$ npm copy-popper
# git commit
```
